const email_random = () => {
    let randomDate = new Date().getTime();
    let dateNow = new Date(randomDate);
    let random_number = dateNow.getDay() + "" + dateNow.getHours() + "" + dateNow.getSeconds() + "" + dateNow.getMilliseconds()
    let email = random_number + "+test" + "@domain.com";
    return email;
};

const ramd = (myarray) => {
    let index = Math.floor(Math.random() * myarray.length);
    return index;
    
}

const milliTime=(days=0)=>{
    const today = new Date();
    today.setDate(today.getDate() + days);
    const startMillis = today.getTime();
    return startMillis;
  }
  

const dateFunction = (pminutes=0, _day,_month,format) => {
    let date = new Date();
    date = new Date(date.setTime(date.getTime() + (pminutes * 60 * 1000)));
    const hour = date.getHours().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const day = _day || date.getDate();
    const month =  _month || (date.getMonth() + 1);
    const year = date.getFullYear();
    const formattedDate = formatDate(format,year,month,day,hour,minutes,seconds);
    return formattedDate;
  };
  
  function formatDate(format,year,month,day,hour,minutes,seconds){
      switch(format) {
         case 'YYYY-MM-DD': {
            return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}`;
         }
         case 'YYYY-MM-DDT': {
            return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`; 
         }
         default: {
              return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`; 
         }
      }  
  }
  

 const decodeB64 = (sessionId) => {
    sessionId = atob(sessionId);
    sessionId = sessionId.split(':')
    sessionId =sessionId[1];
    return sessionId
  }


  const getSingleOrMultipleOption = (value) =>{
    //let typeValue = typeof(value);
    let myValue = value.toString();
    if (myValue.includes('.')){
        myValue = myValue.split('.');
    }else{
        myValue = [value];
    }
    myValue = myValue.map((i) => Number(i));
    return myValue;
}

const test_http_status_multiple_options = (httpStatus,responseCode) =>{
    pm.test(`Status code is [${httpStatus}]`, function () {
        pm.expect(responseCode).to.be.oneOf(httpStatus)
    });
}

const httpStatusValidation = (httpStatus,reponseCode) =>{
    console.log('httpStatus',httpStatus)
    console.log('reponseCode',reponseCode)
    pm.test(`Status code is ${httpStatus}`, function () {
        pm.expect(reponseCode).to.be.oneOf(httpStatus)
    });
}


const test_equal_value = (message,values,value_compare) =>{
    pm.test(`${message}`, function () {
        pm.expect(values).to.eql(value_compare);
    });
}

const validate_schema = (output,Pschema,httpStatus) =>{
    Ajv = require('ajv'),
    ajv = new Ajv({
        logger: console,
        allErrors: true
    });


    pm.test(`Check if schema is valid ${httpStatus}`, function () {
        pm.expect(ajv.validate(Pschema, output),
            JSON.stringify(ajv.errors)).to.be.true;
    });
}
/*
const array_vars = [
    "v_",
    "x_"
    ];
*/
const unsetCollectionVariables = (array_vars) => {
        for (let index = 0; index < elements.length; index++) {
        const element = elements[index];
        cleanup(element);
    }
}


function cleanup(myVar) {
    const clean = _.keys(pm.collectionVariables.toObject())
    _.each(clean, (arrItem) => {
        if (arrItem.startsWith(`${myVar}`)) {
          
            pm.collectionVariables.unset(arrItem)
        }
    })
}

const decodeJwt = (token) =>{ 
  var base64Payload = token.split('.')[1];
  var payload = Buffer.from(base64Payload, 'base64');
  payload = JSON.parse(payload.toString());
  console.log(payload); 
  let data=payload['https://domain.com/properties'];
  return data;
}

function fillArray (v_varArrayName,myObj) {
    let trxArray = [];
   if (pm.collectionVariables.get(`${v_varArrayName}`)){
        trxArray = pm.collectionVariables.get(`${v_varArrayName}`);
   }
   if (trxArray.length > 0){
       trxArray = JSON.parse(trxArray);
   }
   trxArray.push(myObj);
   let newstringObjets = JSON.stringify(trxArray);
   console.log("newsObjets_new",newstringObjets);
   pm.collectionVariables.set(`${v_varArrayName}`, newstringObjets);
   return newstringObjets;
}


function getValueMultiArray (ArrayArg,NAME='blabla', actualDate){
    for (i = 0; i < ArrayArg.length; i++) {
      const {day,sessions} = ArrayArg[i];

      if (day === actualDate){
          for(j = 0; j < sessions.length; j++){
              let {categories} = sessions[j]
              console.log('categories.name',categories[0].name)
              if (categories[0].name === NAME){
                  return sessions[j].id;
              }
          }
      }
   
    }
    return ArrayArg;
}
