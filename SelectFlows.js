const REQ1 = 'REQ1';
const REQ2='REQ2'
const REQ3 ='REQ3'
let index_user = pm.collectionVariables.get("p_index");

//just for debbug
let setStage = [
    {
        "regCode":1,
        "regStatus":"na",
        "httpStatus_request":200.201, 
        "title": "your title test  .... bla bla bla",
        "isFinaltest": "false"
    }
]

//from json data file
let data_set_stage = [
    {
        regCode:data['regCode'] || 'NA',
        regStatus:data['regStatus'] || 'NA',
        httpStatus_request:  data['httpStatus_request'] ||  200.201,
        title: data['title'] || 'N',
        isFinaltest: data['isFinaltest'] || 'NA',
    }
]


// It is used to go through the setStage array if it is going to be executed locally for debbug
//**************************************************** */
if (index_user >= 0 && index_user < setStage.length - 1) { index_user = index_user + 1; } else { index_user = 0; }
//**************************************************** */
const USERSET = data_set_stage[0].title.length > 3 ? data_set_stage[0] : setStage[index_user];


if (USERSET.regStatus !='na'){
    userFlow = REQ3;
}else{
     userFlow = parseInt(USERSET.regCode) > 0 ? REQ2  : REQ1
}

let title_test = USERSET.title+ ' '+ userFlow


//************* */
//unsetCollectionVariables(array_vars);  
setCollectionEnviroments(USERSET);
flow(userFlow);
//************* */


function flow(userFlow){   
    switch(userFlow) {
       case userFlow = REQ2 : {
          console.log("got to REQ2");
          postman.setNextRequest("REQ2");
          break;
       }
        case userFlow = REQ1 : {
          console.log("got to REQ1");
          postman.setNextRequest("REQ1");
          break;
       }
       default: {
          console.log("go to REQ3"); 
          postman.setNextRequest("REQ3");
          break;
       }
    }
}

function setCollectionEnviroments(setStage){
    let {regCode,regStatus_get}= setStage;     
    pm.collectionVariables.set('v_regCode',regCode)
    pm.collectionVariables.set('v_regStatus',regStatus_get)
    pm.collectionVariables.set(`v_httpStatus_request`, httpStatus_request);
    pm.collectionVariables.set('v_title_test',title_test)
    pm.collectionVariables.set(`p_index`, index_user);
}