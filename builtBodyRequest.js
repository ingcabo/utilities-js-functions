
let rq_body = JSON.stringify(createBody(paramA, paramB, paramC, paramD, paramE, paramF))

pm.collectionVariables.set("v_rq_body", rq_body);

/*
{{v_rq_body}}
 */

function createBody(paramA, paramB, paramC, paramD, paramE, paramF) {
    let body = {
        "nameA": `${paramA}`,
        "nameB": `${paramB}`,
        "nameC": `${paramC}`,
        "param_metadata": paraMetadata(paramD, paramE, paramF)
    }

    return body;
}

function paraMetadata(paramD, paramE, paramF){
    let meta = {
        paramD, paramE, paramF
    }
    return meta
}